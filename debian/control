Source: kivy
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders:
 Bastian Venthur <venthur@debian.org>,
 Vincent Cheng <vcheng@debian.org>
Build-Depends:
 cython,
 cython3,
 debhelper (>= 9),
 dh-python,
 libgl1-mesa-dev,
 libgles2-mesa-dev,
 libgstreamer1.0-dev,
 libsdl2-dev,
 libsdl2-image-dev,
 libsdl2-mixer-dev,
 libsdl2-ttf-dev,
 pkg-config,
 python-all-dev (>= 2.7),
 python3-all-dev
Standards-Version: 3.9.8
Homepage: http://kivy.org
Vcs-Git: https://salsa.debian.org/python-team/modules/kivy.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/kivy

Package: python-kivy
Architecture: any
Depends:
 python-gst-1.0,
 python-pygame,
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Description: Kivy - Multimedia / Multitouch framework in Python (Python 2)
 Kivy is an open source library for developing multi-touch applications. It is
 completely cross platform (Linux/OSX/Win/Android) and released under the terms
 of the MIT license.
 .
 It comes with native support for many multi-touch input devices, a growing
 library of multi-touch aware widgets, hardware accelerated OpenGL drawing, and
 an architecture that is designed to let you focus on building custom and highly
 interactive applications as quickly and easily as possible.
 .
 Kivy is a mixed Python library with Cython code, to take advantage of its
 highly dynamic nature and use any of the thousands of high quality and open
 source Python libraries out there, with the speed of C code.
 .
 This package contains the Python 2 compatible version of the library.

Package: python3-kivy
Architecture: any
Depends:
 python3-gst-1.0,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Description: Kivy - Multimedia / Multitouch framework in Python (Python 3)
 Kivy is an open source library for developing multi-touch applications. It is
 completely cross platform (Linux/OSX/Win/Android) and released under the terms
 of the MIT license.
 .
 It comes with native support for many multi-touch input devices, a growing
 library of multi-touch aware widgets, hardware accelerated OpenGL drawing, and
 an architecture that is designed to let you focus on building custom and highly
 interactive applications as quickly and easily as possible.
 .
 Kivy is a mixed Python library with Cython code, to take advantage of its
 highly dynamic nature and use any of the thousands of high quality and open
 source Python libraries out there, with the speed of C code.
 .
 This package contains the Python 3 compatible version of the library.

Package: python-kivy-examples
Architecture: all
Depends: python-kivy, ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
Description: Kivy - Multimedia / Multitouch framework in Python (examples)
 Kivy is an open source library for developing multi-touch applications. It is
 completely cross platform (Linux/OSX/Win/Android) and released under the terms
 of the MIT license.
 .
 It comes with native support for many multi-touch input devices, a growing
 library of multi-touch aware widgets, hardware accelerated OpenGL drawing, and
 an architecture that is designed to let you focus on building custom and highly
 interactive applications as quickly and easily as possible.
 .
 Kivy is a mixed Python library with Cython code, to take advantage of its
 highly dynamic nature and use any of the thousands of high quality and open
 source Python libraries out there, with the speed of C code.
 .
 This package includes all examples which can be found in the git repository.
